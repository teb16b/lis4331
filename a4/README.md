# LIS4331 Adv. Mobile App Development

## Tyler Bartlett

### Assignment 4 Requirements:

*Sub-Heading:*

1. Include splash screen image (or, create your own), app title, intro text.
2. Include appropriate images.
3. Must use persistent data: SharedPreferences
4. Widgets and images must be vertically and horizontally aligned.
5. Must add background color(s) or theme
6. Create and display launcher icon image

#### README.md file should include the following items:

1. Skillset(s) 10, 11, 12
2. Mortgage Calculator App
	- Valid Screen
	- Invalid Screen
	- Splash Screen
	- Main Screen

#### Assignment Screenshots:

**Screenshots Of App**:

![Splash]
(img/splash.png)
![Main]
(img/main.png)
![Correct]
(img/correct.png)
![Invalid]
(img/invalid.png)
![GIF Example]
(img/phonegif.gif)

**Skillset 10**:

![Skillset 10]
(img/skill10.png)

**Skillset 11**:

![Skillset 11]
(img/skill11.png)

**Skillset 12**:

![Skillset 12]
(img/skill12.png)