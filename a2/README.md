> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Adv. Mobile App Development

## Tyler Bartlett

### Assignment 2 Requirements:

*Sub-Heading:*

1. Skillset 1
2. Skillset 2
3. Skillset 3
4. Tipping calculator app

#### README.md file should include the following items:

* Skillset 1
* Skillset 2
* Skillset 3
* Screenshot of unpopulated interface
* Screenshot of populated tables
* GIF of app running (cause why not) 

#### Assignment Screenshots:

*Screenshots of App*:

![Unpopulated interface]
(img/unpop.png)
![Populated Interface]
(img/pop.png)
![GIF of working app]
(img/tut.gif)

**Screenshot of Skillset 1**
>
>
![Skillset 1] (img/skillset1.png)

**Screenshot of Skillset 2**
>
>
![Skillset 2] (img/skillset2.png)

**Screenshot of Skillset 3**
>
>
![Skillset 3] (img/skillset3.png)