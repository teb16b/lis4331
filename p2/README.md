# LIS 4331 ADV Mobile App Development 

## Tyler Bartlett

### Project 2 Requirements:

*Sub-Heading:*

1. Splash Screen
2. Persistent Data
3. Five Users
4. Background or theme
5. Launcher Icon

#### README.md file should include the following items:

* Screenshots of running application
* Course title, name, requirements

#### Assignment Screenshots:

**Screenshot of APP**

![Splash Screenshot]
(img/splash.png)
![Add Screenshot]
(img/add.png)
![Update Screenshot]
(img/update.png)
![Delete Screenshot]
(img/delete.png)


