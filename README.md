> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Adv. Mobile App Development

## Tyler Bartlett

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    * Screenshot of running JDK java hello
    * Screenshot of running Android Studio My First App
    * Screenshots of running Android Studio Contacts App
    * Git commands with short descriptions
    * Bitbucket repo links

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Skillset 1
    * Skillset 2
    * Skillset 3
    * Tipping calculator app
3. [A3 README.md](a3/README.md "My A3 README.md file")
	* Field to enter U.S. dollar amount: 1–100,000
	* Must include toast notification if user enters out-of-range values
	* Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
	* Must include correct sign for euros, pesos, and Canadian dollars
	* Must add background color(s) or theme
	* Create and displaylauncher icon image
	* Create Splash/Loading Screen:
4. [A4 README.md](a4/README.md "My A4 README.md file")
	* Include splash screen image (or, create your own), app title, intro text.
	* Include appropriate images.
	* Must use persistent data: SharedPreferences
	* Widgets and images must be vertically and horizontally aligned.
	* Must add background color(s) or theme
	* Create and display launcher icon image
5. [A5 README.md](a5/README.md "My A5 README.md file")
	* Main screen with app title and list of articles
	* Use your own RSS feed.
	* Must add background color(s) or theme
	* Create and display launcher icon image
6. [P1 README.md](p1/README.md "My P1 README.md file")
	* Screenshot of splash screen
	* Screenshot of main screen (playing)
	* Screenshot of main screen (paused)
7. [P2 README.md](p2/README.md "My P2 README.md file")
	* Screenshot of Splash, Add, Delete, and Update user
