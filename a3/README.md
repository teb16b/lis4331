> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331 Adv. Mobile App Development

## Tyler Bartlett

### Assignment 3 Requirements:

*Sub-Heading:*

1. Field to enter U.S. dollar amount: 1–100,000
2. Must include toast notification if user enters out-of-range values
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must be vertically and horizontally aligned)
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must add background color(s) or theme
6. Create and displaylauncher icon image
7. Create Splash/Loading Screen:

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshot of running application’s splash screen
* Screenshot of running application’s unpopulateduser interface
* Screenshot of running application’s toast notification
* Screenshotofrunning application’sconverted currencyuser interface


#### Assignment Screenshots:

**Screenshot of Splash Screen**:

![Splash Screen Screenshot]
(img/splash.png)
![Splash Screen Gif]
(img/splashmov.gif)

**Screenshot of running Currency Converter App**:

![Screenshot of Blank App]
(img/blank.png)
![Screenshot of Toast Noti]
(img/toast.png)
![Screenshot of Filled App]
(img/filled.png)
![Test run of app]
(img/testrun.gif)

**Screenshot of Skillset 4**:

![Screenshot of Skillset 4]
(img/skillset4.png)


**Screenshot of Skillset 5**:

![Screenshot of Skillset 5]
(img/skillset5.png)

![Screenshot of Skillset 5 Dialog Boxes]
(img/dialog1.png)
![Screenshot of Skillset 5 Dialog Boxes]
(img/dialog2.png)
![Screenshot of Skillset 5 Dialog Boxes]
(img/dialog3.png)
![Screenshot of Skillset 5 Dialog Boxes]
(img/dialog4.png)
![Screenshot of Skillset 5 Dialog Boxes]
(img/dialog5.png)

**Screenshot of Skillset 6**:

![Screenshot of Skillset 6]
(img/skillset6.png)

![Screenshot of Skillset 6 Dialog Boxes]
(img/dialog1_ss6.png)
![Screenshot of Skillset 6 Dialog Boxes]
(img/dialog2_ss6.png)
![Screenshot of Skillset 6 Dialog Boxes]
(img/dialog3_ss6.png)
![Screenshot of Skillset 6 Dialog Boxes]
(img/dialog4_ss6.png)
![Screenshot of Skillset 6 Dialog Boxes]
(img/dialog5_ss6.png)