# LIS4331 Adv. Mobile App Development

## Tyler Bartlett

### Assignment 5 Requirements

*Sub-Heading:*

1. Main screen with app title and list of articles
2. Use your own RSS feed.
3. Must add background color(s) or theme
4. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of main screen
* Screenshot of individual article 
* Screenshot of default browser

#### Assignment Screenshots:

**Screenshot of app:**

![App Screenshot]
(img/screen.png)
![App Screenshot]
(img/article.png)
![App Screenshot]
(img/web.png)

**Skillset 13**:

![ss13]
(img/ss13.png)

**Skillset 14**:

![ss14]
(img/ss14.png)

**Skillset 15**

![ss15]
(img/ss15.png)

