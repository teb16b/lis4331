> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4331 ADV Mobile App Development 

## Tyler Bartlett

### Assignment 1 Requirements:

*Sub-Heading:*

1. Screenshot of running JDK java hello
2. Screenshot of running Android Studio My First App
3. Screenshots of running Android Studio Contacts App
4. Git commands with short descriptions
5. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of running JDK java hello
* Screenshot of running Android Studio My First App
* Screenshots of running Android Studio Contacts App
* Git commands with short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init = Create an empty Git repository or reinitialize an existing one
2. git status = Show the working tree status
3. git add = Add file contents to the index
4. git commit = Record changes to the repository
5. git push = Update remote refs along with associated objects
6. git pull = Fetch from and integrate with another repository or a local branch
7. git log = Show commit logs

#### Assignment Screenshots:
*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

*Screenshots of Contact App*:
>
>

![Android Studio Contacts App]
(img/contactapp1.png) 
![Android Studio App]
(img/contactapp2.png)
![Android Studio GIF]
 (img/contactex.gif)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
