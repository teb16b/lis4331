# LIS 4331 ADV Mobile App Development 

## Tyler Bartlett

### Project 1 Requirements:

1. Include splash screen image, app title, intro text.
2. Include artists’ images and media.
3. Images and buttons must be vertically and horizontally aligned.
4. Must add background color(s) or theme
5. Create and display launcher icon image

#### README.md file should include the following items:

* Screenshot of splash screen
* Screenshot of main screen (playing)
* Screenshot of main screen (paused)
* Skillsets 7-9

#### Assignment Screenshots:

**Screenshots of App**:

![Splash Screen]
(img/splash.png)
![Screenshot of Playing]
(img/play.png)
![Screenshot of Paused]
(img/pause.png)

**Example of App Running (GIF)**

![Animated GIF of App Running]
(img/example.gif)

# **Video**
![<---  Right Click Broken IMG Icon > Open Image in New Tab to View Video]
(img/video.mp4)

**Skillset 7**
>
>
![Skillset]
(img/ss7.png)

**Skillset 8**
>
>
![Skillset]
(img/ss8.png)
![Skillset]
(img/ss8_pop.png)

**Skillset 9**
>
>
![Skillset]
(img/ss9.png)
![Skillset]
(img/ss9_pop.png)
